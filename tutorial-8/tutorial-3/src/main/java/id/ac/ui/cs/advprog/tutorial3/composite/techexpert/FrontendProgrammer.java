package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class FrontendProgrammer extends Employees {
    public FrontendProgrammer(String name, double salary) {
        this.name = name;
        this.salary = salary;
        this.role = "Front End Programmer";
        if (salary < 30000) {
            throw new IllegalArgumentException("FrontendProgrammer Salary must not lower than 30000.00");
        }
    }

    @Override
    public double getSalary() {
        return this.salary;
    }
}
